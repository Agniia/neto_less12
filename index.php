<?php
	header('Content-Type: text/html; charset=utf-8');
    require_once(__DIR__.'/config.php');
    
    $query = 'select * from books';   
    $result =  mysqli_query($link , $query);
 ?>
 <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Books</title>
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>   
    <h1 style="margin: 80px auto 70px; text-align: center;">Список книг</h1>
<?php    
	echo '<table class="table table-dark">';    
    echo '<thead>';    
	echo '<td>Номер</td>';
	echo '<td>Название</td>';
	echo '<td>Автор</td>';
	echo '<td>Год</td>';
	echo '<td>isbn</td>';
	echo '<td>Жанр</td>';    
	echo '</thead>';    
	echo '<tbody>';    
    while($data =  mysqli_fetch_assoc($result))
    {
    	echo '<tr>';    
    	echo '<td>'.$data['id'].'</td>';
    	echo '<td>'.$data['name'].'</td>';
    	echo '<td>'.$data['author'].'</td>';
    	echo '<td>'.$data['year'].'</td>';
    	echo '<td>'.$data['isbn'].'</td>';
    	echo '<td>'.$data['genre'].'</td>';    
    	echo '</tr>';    
    }
   echo '</tbody>';   
   echo '</table>';
 ?>
 </body>   
 </html>